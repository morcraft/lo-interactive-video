const _ = require('lodash')
const playStates = require('./playStates.js')
const addInteractiveVideoSteps = require('./addInteractiveVideoSteps.js')

module.exports = function (args) {
    var self = this
    self.evaluate = function(_args){
        if (!_.isObject(_args)) 
            throw new Error('Some arguments are expected.')

        if(!(_args.event instanceof Event))
            throw new Error('An event is expected.')

        if(!_.isNumber(_args.value))
            throw new Error('A number was expected as a state value')

        if(!_.isObject(_args.state))
            throw new Error('A state is expected.')

        if(!_.isObject(_args.videoSteps))
            throw new Error('Some video steps are expected.')
    
        if (_args.value == playStates.PLAYING) {
            if (_.isNumber(self.playInterval)) {
                clearInterval(self.playInterval)
            }
    
            self.playInterval = setInterval(function () {
                addInteractiveVideoSteps({
                    event: _args.event,
                    videoSteps: _args.videoSteps
                })
            }, 200)
        } 
        
        else {
            if (_args.value === playStates.PAUSED) {
                if (_.isNumber(self.playInterval)) {
                    clearInterval(self.playInterval)
                }
            }
        }
    }
}